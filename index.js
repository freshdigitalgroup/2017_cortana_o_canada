/**
 * @fileoverview Includes Cortana functions for Bot Framework.
 * @author Ke Shao
 */

const builder = require('botbuilder');

require('dotenv').config()

// Create chat connector for communicating with the Bot Framework Service
let connector = new builder.ChatConnector({
  appId: process.env.APP_ID,
  appPassword: process.env.APP_PASSWORD
});


// Redirects messages to Microsoft.Launch Dialog
let bot = new builder.UniversalBot(connector, function(session) {
  session.beginDialog('Microsoft.Launch');
});

// Dialog that plays audio
bot.dialog('Microsoft.Launch', function(session) {
  for(var x in session) {
    console.log(x, session[x]);
  }
  session.say('Playing O Canada', '<speak> <audio src=\'https://d12y3e7lbn1vso.cloudfront.net/assets/audio/National_Anthem_O_Canada.mp3\'/> </speak>')
  session.endConversation();
});

/**
 * Configures Bot Framework for AWS Lambda
 * @param {obj} connector configuration for bot
 * @return {obj} handler
 */
function lambda(connector) {
  let listener = connector.listen();
  let handler = (event, context, callback) => {
    let reqWrapper = {
      body: event.body,
      headers: event.headers
    };
    let statusCode;
    let resWrapper = {
      status: (code) => {
        statusCode = code;
      },
      end: () => {
        callback(null, {
          statusCode: statusCode
        });
      }
    };
    listener(reqWrapper, resWrapper);
  };
  return handler;
}

exports.handler = lambda(connector);
